﻿namespace Middleware
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txtPhysicalPath = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.fbdShowFolders = new System.Windows.Forms.FolderBrowserDialog();
            this.btnShowFolders = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.çıkışToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.çıkışToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.lstWarnings = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tmrReadTextFile = new System.Windows.Forms.Timer(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.chkReadActive = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lstCurrentActions = new System.Windows.Forms.ListBox();
            this.pnlWarning = new System.Windows.Forms.Panel();
            this.lblWarning = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.pnlWarning.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtPhysicalPath
            // 
            this.txtPhysicalPath.Location = new System.Drawing.Point(30, 116);
            this.txtPhysicalPath.Name = "txtPhysicalPath";
            this.txtPhysicalPath.Size = new System.Drawing.Size(397, 20);
            this.txtPhysicalPath.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.Location = new System.Drawing.Point(27, 95);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(194, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "Okunacak dosyanın fiziksel adresi:";
            // 
            // btnShowFolders
            // 
            this.btnShowFolders.Location = new System.Drawing.Point(434, 114);
            this.btnShowFolders.Name = "btnShowFolders";
            this.btnShowFolders.Size = new System.Drawing.Size(75, 23);
            this.btnShowFolders.TabIndex = 2;
            this.btnShowFolders.Text = "Gözat";
            this.btnShowFolders.UseVisualStyleBackColor = true;
            this.btnShowFolders.Click += new System.EventHandler(this.btnShowFolders_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.çıkışToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(940, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "mnuMainMenu";
            // 
            // çıkışToolStripMenuItem
            // 
            this.çıkışToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.çıkışToolStripMenuItem1});
            this.çıkışToolStripMenuItem.Name = "çıkışToolStripMenuItem";
            this.çıkışToolStripMenuItem.Size = new System.Drawing.Size(51, 20);
            this.çıkışToolStripMenuItem.Text = "Dosya";
            // 
            // çıkışToolStripMenuItem1
            // 
            this.çıkışToolStripMenuItem1.Name = "çıkışToolStripMenuItem1";
            this.çıkışToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.çıkışToolStripMenuItem1.Text = "Çıkış";
            // 
            // lstWarnings
            // 
            this.lstWarnings.FormattingEnabled = true;
            this.lstWarnings.Location = new System.Drawing.Point(30, 228);
            this.lstWarnings.Name = "lstWarnings";
            this.lstWarnings.Size = new System.Drawing.Size(397, 147);
            this.lstWarnings.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label2.Location = new System.Drawing.Point(27, 207);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 15);
            this.label2.TabIndex = 5;
            this.label2.Text = "Önemli uyarılar:";
            // 
            // tmrReadTextFile
            // 
            this.tmrReadTextFile.Enabled = true;
            this.tmrReadTextFile.Interval = 10000;
            this.tmrReadTextFile.Tick += new System.EventHandler(this.tmrReadTextFile_Tick);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label3.Location = new System.Drawing.Point(619, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "Ayarlar";
            // 
            // chkReadActive
            // 
            this.chkReadActive.AutoSize = true;
            this.chkReadActive.Location = new System.Drawing.Point(622, 124);
            this.chkReadActive.Name = "chkReadActive";
            this.chkReadActive.Size = new System.Drawing.Size(103, 17);
            this.chkReadActive.TabIndex = 7;
            this.chkReadActive.Text = "Okuma Aktif mi?";
            this.chkReadActive.UseVisualStyleBackColor = true;
            this.chkReadActive.CheckedChanged += new System.EventHandler(this.chkReadActive_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label4.Location = new System.Drawing.Point(456, 207);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 15);
            this.label4.TabIndex = 9;
            this.label4.Text = "Şu anki işlemler:";
            // 
            // lstCurrentActions
            // 
            this.lstCurrentActions.FormattingEnabled = true;
            this.lstCurrentActions.HorizontalScrollbar = true;
            this.lstCurrentActions.Location = new System.Drawing.Point(459, 228);
            this.lstCurrentActions.Name = "lstCurrentActions";
            this.lstCurrentActions.Size = new System.Drawing.Size(397, 147);
            this.lstCurrentActions.TabIndex = 8;
            // 
            // pnlWarning
            // 
            this.pnlWarning.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.pnlWarning.Controls.Add(this.lblWarning);
            this.pnlWarning.Location = new System.Drawing.Point(30, 39);
            this.pnlWarning.Name = "pnlWarning";
            this.pnlWarning.Size = new System.Drawing.Size(565, 44);
            this.pnlWarning.TabIndex = 10;
            this.pnlWarning.Visible = false;
            // 
            // lblWarning
            // 
            this.lblWarning.AutoSize = true;
            this.lblWarning.Location = new System.Drawing.Point(16, 15);
            this.lblWarning.Name = "lblWarning";
            this.lblWarning.Size = new System.Drawing.Size(31, 13);
            this.lblWarning.TabIndex = 0;
            this.lblWarning.Text = "Uyarı";
            this.lblWarning.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(940, 450);
            this.Controls.Add(this.pnlWarning);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lstCurrentActions);
            this.Controls.Add(this.chkReadActive);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lstWarnings);
            this.Controls.Add(this.btnShowFolders);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtPhysicalPath);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Geçiş Aktarım Programı";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.pnlWarning.ResumeLayout(false);
            this.pnlWarning.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtPhysicalPath;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.FolderBrowserDialog fbdShowFolders;
        private System.Windows.Forms.Button btnShowFolders;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem çıkışToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem çıkışToolStripMenuItem1;
        private System.Windows.Forms.ListBox lstWarnings;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Timer tmrReadTextFile;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox chkReadActive;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListBox lstCurrentActions;
        private System.Windows.Forms.Panel pnlWarning;
        private System.Windows.Forms.Label lblWarning;
    }
}

