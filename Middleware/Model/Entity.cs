﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Middleware.Model
{
    class Entity
    {
        public class Transaction
        {
            public int direction { get; set; }
            public bool input { get; set; }
            public int origin { get; set; }
            public string motion_cause_id { get; set; }
            public string terminal_id { get; set; }
            public string employee_id { get; set; }
            public string card_no { get; set; }
            public string card_id { get; set; }
            public string company_id { get; set; }
            public string zone_id { get; set; }
            public DateTime read_date { get; set; }
        }
    }
}
