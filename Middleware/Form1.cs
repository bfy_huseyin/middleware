﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace Middleware
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            lstWarnings.Items.Insert(0, "Program başlatıldı. (" + DateTime.Now.ToString() + ")");
        }

        private void btnShowFolders_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    //string[] files = Directory.GetFiles(fbd.SelectedPath);
                    lstWarnings.Items.Add("Fiziksel dosya yolu seçildi. (" + DateTime.Now.ToString() + ")");

                    string PhysicalPath = fbd.SelectedPath;
                    txtPhysicalPath.Text = PhysicalPath;

                    var TerminalList = Directory.GetFiles(PhysicalPath);
                    lstWarnings.Items.Add(TerminalList.Count() + " adet taranabilecek terminal bulundu. (" + DateTime.Now.ToString() + ")");
                }
            }
        }

        private void tmrReadTextFile_Tick(object sender, EventArgs e)
        {
            if (chkReadActive.Checked)
            {
                if (!String.IsNullOrEmpty(txtPhysicalPath.Text) && txtPhysicalPath.Text.Length > 0)
                {
                    tmrReadTextFile.Interval = 30000;

                    lblWarning.Visible = false;
                    lblWarning.Text = "";
                    pnlWarning.Visible = false;

                    List<Transaction> TransactionList = new List<Transaction>();
                    lstCurrentActions.Items.Clear();
                    string HardwareType = String.Empty;
                    var dummy = "20170710,123458,0,,00051541490900,00,0,2,,01";
                    int CommaCount = dummy.Split(',').Count();

                    #region Donanım Seçme
                    switch (CommaCount) // Bu alanda ne kadar donanım kullanıyorsak hepsinin tipi gelmeli.
                    {
                        case 10:
                            HardwareType = "ax_gate";
                            break;
                        case 11:
                            // XXX
                            break;
                        default:
                            lstCurrentActions.Items.Add("HATA: .txt dosya formatı tanınmadı.");
                            lstWarnings.Items.Add("HATA: .txt dosya formatı tanınmadı. (" + DateTime.Now.ToString() + ")");
                            break;
                    }
                    #endregion

                    var TerminalList = Directory.GetFiles(txtPhysicalPath.Text);

                    if (!TerminalList.Contains("offline")) // Adında ofline geçmeyen bütün terminalleri okuyor
                    {
                        foreach (var terminal in TerminalList) // terminal: "TRANSACTION_2181.txt
                        {
                            lstCurrentActions.Items.Add(terminal + " terminal okunuyor.");
                            var text = "";
                            try
                            {
                                text = File.ReadAllText(terminal); // "text: 20171020,123113,1,,00051541086535,00,0,1,,01"
                                File.WriteAllText(terminal, "");
                            }
                            catch (Exception)
                            {
                                lstCurrentActions.Items.Add(terminal + " dosya kullanımda olduğu için kullanılamadı.");
                                continue;
                            }

                            if (text.Length > 0)
                            {
                                try
                                {
                                    string terminal_string = terminal.Split('_').Last();
                                    string terminal_id = terminal_string.Split('.').First();

                                    #region İsim kontrolü
                                    if (!terminal.Contains("TRANSACTION_")) // "TRANSACTION_" isimlendirmesi dosyada geçmiyorsa atlıyor
                                    {
                                        continue;
                                    }
                                    #endregion

                                    var textlines = text.Split('\n');
                                    int writecount = 0; // Database'ye aktarılan kayıt sayısı
                                    foreach (var newline in textlines)
                                    {
                                        //Son satır boş ise atla
                                        if (newline.Length == 0) continue;

                                        var textsplit = newline.Split(',');

                                        switch (HardwareType)
                                        {
                                            case "ax_gate":
                                                // BU DONANIM TİPİ İÇİN GELEN FORMAT: 20170710,123458,0,,00051541490900,00,0,2,,01

                                                #region Ax Gate
                                                // string CardNo = textsplit[4];
                                                // int Direction = int.Parse(textsplit[7]);
                                                // DateTime ReadDate = DateTime.ParseExact(sample, formatString, null);
                                                // string GateID = terminal_id;

                                                writecount++;
                                                Transaction new_transaction = new Transaction();
                                                new_transaction.card_no = textsplit[4];
                                                new_transaction.direction = int.Parse(textsplit[7]);

                                                string formatString = "yyyyMMddHHmmss";
                                                string sample = textsplit[0] + textsplit[1];
                                                new_transaction.read_date = DateTime.ParseExact(sample, formatString, null);
                                                new_transaction.terminal_id = terminal_id;
                                                //new_transaction.input = "";
                                                //new_transaction.origin = "";

                                                DateTime CurrentDate = DateTime.Now;
                                                TimeSpan CurrentTime = new TimeSpan(DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
                                                #endregion

                                                TransactionList.Add(new_transaction);



                                                text = text.Replace(newline + "\n", "");
                                                break;
                                            case "XXX":
                                                // XXX
                                                break;
                                            default:
                                                lstCurrentActions.Items.Add("HATA: .txt dosya formatında geçersiz satır algılandı.");
                                                lstWarnings.Items.Add("HATA: .txt dosya formatında geçersiz satır algılandı.");
                                                break;
                                        }
                                    }

                                    //HttpClient httpClient = new HttpClient();
                                    // http://localhost:13505/api/transaction/read_transaction
                                    var json_data = JsonConvert.SerializeObject(TransactionList);

                                    #region Çalışıyor fakat apide veriyi alamıyorum
                                    //using (var wb = new WebClient())
                                    //{
                                    //    var data = new NameValueCollection();
                                    //    data["username"] = "myUser";
                                    //    data["password"] = "myPassword";

                                    //    var response = wb.UploadString("http://localhost:13505/api/transaction/read_transaction", "GET", json_data);
                                    //    //string responseInString = Encoding.UTF8.GetString(response);
                                    //}
                                    #endregion

                                    #region Çalışıyor fakat apide veriyi alamıyorum
                                    //HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create("http://localhost:13505/api/transaction/read_transaction");
                                    //httpWebRequest.ContentType = "application/json; charset=utf-8";
                                    //httpWebRequest.Method = "POST";

                                    //using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                                    //{
                                    //    string json_data = "[  { \"ReferenceId\": \"a123\"  } ]";
                                    //    Debug.Write(json_data);
                                    //    streamWriter.Write(json_data);
                                    //    streamWriter.Flush();
                                    //    streamWriter.Close();
                                    //}
                                    //try
                                    //{
                                    //    using (var response = httpWebRequest.GetResponse() as HttpWebResponse)
                                    //    {
                                    //        if (httpWebRequest.HaveResponse && response != null)
                                    //        {
                                    //            using (var reader = new StreamReader(response.GetResponseStream()))
                                    //            {
                                    //                var result = reader.ReadToEnd();
                                    //            }
                                    //        }
                                    //    }
                                    //}
                                    //catch (WebException ex)
                                    //{
                                    //    if (ex.Response != null)
                                    //    {
                                    //        using (var errorResponse = (HttpWebResponse)ex.Response)
                                    //        {
                                    //            using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                                    //            {
                                    //                string error = reader.ReadToEnd();
                                    //                var result = error;
                                    //            }
                                    //        }

                                    //    }
                                    //}
                                    #endregion


                                    HttpClient client = new HttpClient();

                                    //var content = new StringContent("asd_aq", Encoding.UTF8, "application/json");

                                    StringContent content = new StringContent(JsonConvert.SerializeObject(json_data), Encoding.UTF8, "application/json");
                                    var result = client.PostAsync("http://localhost:13505/api/transaction/read_transaction", content).Result;


                                    lstCurrentActions.Items.Add(writecount + " adet kayıt web api'ye post edildi.");
                                }
                                catch (Exception ex)
                                {
                                    lstCurrentActions.Items.Add("HATA: " + terminal + "'de hata oluştu.");
                                    lstWarnings.Items.Add("HATA: " + terminal + "'de hata oluştu.");
                                }

                                lstCurrentActions.Items.Add("Terminal tamamlandı.");
                            }
                            else // Terminale ait txt dosyasında kayıt yoksa
                            {
                                lstCurrentActions.Items.Add(terminal + " numaralı terminale ait txt dosyası boş.");
                            }
                        }
                    }
                }
                else
                {
                    lblWarning.Visible = true;
                    lblWarning.Text = "HATA: Fiziksel dosya adresi belirtilmedi.";
                    pnlWarning.Visible = true;

                    var LastItem = (lstWarnings.Items[lstWarnings.Items.Count - 1]).ToString();
                    if (!LastItem.Contains("HATA: Fiziksel dosya adresi belirtilmedi."))
                    {
                        lstWarnings.Items.Add("HATA: Fiziksel dosya adresi belirtilmedi. (" + DateTime.Now.ToString() + ")");
                    }
                }
            }
        }

        private void chkReadActive_CheckedChanged(object sender, EventArgs e)
        {
            tmrReadTextFile.Enabled = chkReadActive.Checked;
        }

        #region CLASS
        public class Transaction
        {
            public int direction { get; set; }
            public bool input { get; set; }
            public int origin { get; set; }
            public string motion_cause_id { get; set; }
            public string terminal_id { get; set; }
            public string employee_id { get; set; }
            public string card_no { get; set; }
            public string card_id { get; set; }
            public string company_id { get; set; }
            public string zone_id { get; set; }
            public DateTime read_date { get; set; }
        }
        #endregion
    }
}
